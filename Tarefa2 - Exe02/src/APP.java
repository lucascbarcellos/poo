import java.util.Scanner;

public class APP {
    
    //Constante
    static final int MAXCONTA = 20;
   
    //variável comum
    static int index = 0;
    
    //Lista de contas
    static Conta[] lista = new Conta[MAXCONTA];
    
    static Scanner tecla = new Scanner(System.in);

    public static void main(String[] args) {
        int op;
        do {                
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Incluir conta");
            System.out.println("2-Excluir conta");
            System.out.println("3-Sacar");
            System.out.println("4-Depositar");
            System.out.println("5-Listar saldo das Contas");
            System.out.println("6-Consultar saldo de conta");
            System.out.println("7-Transferir");
            System.out.println("8-Sair");
            System.out.println("Digite sua op��o: ");
            op = tecla.nextInt(); 
            switch(op){
                case 1: incluirConta(); break;
                case 2: excluirConta(); break;
                case 3: sacarValor(); break;
                case 4: depositarValor(); break;    
                case 5: listarContas(); break;
                case 6: consultarSaldo(); break;
                case 7: transferir(); break;
                case 8: break;
            }
        } while (op!=8);       
    }
    
    
    public static void incluirConta(){
        //Entrada
        System.out.println("Digite o saldo da conta:");
        double saldo = tecla.nextDouble();
        //Criar o objeto e inserir na lista
        lista[index++] = new Conta(saldo);
        System.out.println("Conta cadastrada com sucesso!");
    }
    
    public static void excluirConta(){
    	 System.out.println("Digite o numero da conta:");
         int num = tecla.nextInt();
         //Procurar a conta na lista
         for (int i = 0; i < lista.length-1; i++) {
             if (num == lista[i].getNumero()){
                 lista[i] = null;
                 System.gc();
                 break;
             }
         }
    }
    
    public static void sacarValor(){
        //Entrada
        System.out.println("Digite o numero da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o valor do saque:");
        double valor = tecla.nextDouble();
        
        //Procurar a conta na lista
        for (int i = 0; i < lista.length-1; i++) {
            if (num == lista[i].getNumero()){
                lista[i].sacar(valor);
                break;
            }
        }
    }
    
    public static void depositarValor(){
        //Entrada
        System.out.println("Digite o numero da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o valor do deposito:");
        double valor = tecla.nextDouble();
        
        //Procurar a conta na lista
        for (int i = 0; i < lista.length-1; i++) {
            if (num == lista[i].getNumero()){
                lista[i].depositar(valor);
                break;
            }
        }
    }
    
    public static void consultarSaldo(){
    	 System.out.println("Digite o numero da conta:");
         int num = tecla.nextInt();
         //Procurar a conta na lista
         for (int i = 0; i < lista.length-1; i++) {
             if (num == lista[i].getNumero()){
            	 System.out.println(lista[i].getNumero()
                         + "........" +
                         lista[i].getSaldo());
                 break;
             }
         }
    }
    
    public static void transferir(){
    	System.out.println("Digite o numero da conta de origem:");
        int num = tecla.nextInt();
        System.out.println("Digite o numero da conta de destino:");
        int num2 = tecla.nextInt();
        System.out.println("Digite o valor da transferencia:");
        double valor = tecla.nextDouble();
        
      //Procurar a conta na lista para retirada e sacar
        for (int i = 0; i < lista.length-1; i++) {
            if (num == lista[i].getNumero()){
                lista[i].sacar(valor);
                break;
            }
        }
        
      //Procurar a conta na lista para entrada da transferencia e depositar
        for (int i = 0; i < lista.length-1; i++) {
            if (num2 == lista[i].getNumero()){
                lista[i].depositar(valor);
                break;
            }
        }
        
    	
    }
    
    public static void listarContas(){
        double total = 0;
        System.out.println("N� Conta:........ SALDO:");
        for (int i = 0; i < lista.length-1; i++) {
            if (lista[i] != null){
                System.out.println(lista[i].getNumero()
                                   + "............" +
                                   lista[i].getSaldo());
                //total += lista[i].getSaldo();
                total = total + lista[i].getSaldo();
            }else{
                break;
            }
        }
        System.out.println("Total:........" + total);
    }
    
    
}
