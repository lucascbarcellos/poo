import java.util.Scanner;

public class APP {

	static Candidato candidatos[] = new Candidato[3];

	static Scanner tecla = new Scanner(System.in);

	public static void main(String[] args) {
		int op;
		do {
			System.out.println("--MENU--");
			System.out.println("1- Cadastrar");
			System.out.println("2- Votar");
			System.out.println("3- Apurar");
			System.out.println("4- Sair");

			op = tecla.nextInt();
			switch (op) {
			case 1:
				incluir();
				break;
			case 2:
				votar();
				break;
			case 3:
				apurar();
				break;
			case 4:
				break;
			}
		} while (op != 4);
	}

	public static void incluir() {
		for (int i = 0; i < 3; i++) {
			System.out.println("Digite o c�digo:");
			int cod = tecla.nextInt();
			System.out.println("Digite o nome:");
			String nome = tecla.next();
			candidatos[i] = new Candidato(cod, nome);
		}
	}

	public static void votar() {
		System.out.println("Digite o c�digo do candidato:");
		int cod = tecla.nextInt();
		for (int i = 0; i < 3; i++) {
			if (candidatos[i].getNumero() == cod) {
				candidatos[i].incrementarVoto();
			}
		}
	}

	public static void apurar() {
		int total_votos = 0;

		for (int i = 0; i < 3; i++) {
			total_votos += candidatos[i].getVotos();
		}
		for (int i = 0; i < 3; i++) {
			System.out.println(candidatos[i].getNome() + " - " + (candidatos[i].getVotos() * 100) / total_votos + "%");
		}
	}
}
