class produto { // (a)
	private static int prox_cod = 1;
	int codigo;
	float preco, peso;

	produto(float preco, float peso) { // (b)
		codigo = prox_cod++;
		this.preco = preco;
		this.peso = peso;
	}

	float getPreco() {
		return preco;
	}

	float getPeso() {
		return peso;
	}

	float getCodigo() {
		return codigo;
	}

	public String toString() { // (e )
		String resp = "codigo: " + codigo + " preco: " + preco + " peso: " + peso;
		return resp;
	}
}

// outro

class mochila { // (a)
	private float peso_atual, gasto_atual;
	float peso, gasto;
	produto[] vetor;
	int num_prod;

	mochila(float p, float g, int l) { // (b)
		peso = p;
		gasto = g;
		peso_atual = gasto_atual = 0;
		vetor = new produto[l];
		num_prod = 0;
	}

	void insere(produto p) { // (c)
		if (((gasto_atual + p.getPreco()) <= gasto) && ((peso_atual + p.getPeso()) <= peso)
				&& (num_prod != vetor.length)) {
			gasto_atual += p.getPreco();
			peso_atual += p.getPeso();
			num_prod++;
			for (int i = 0; i < vetor.length; i++)
				if (vetor[i] != null) {
					vetor[i] = p;
					break;
				}
		} else {
			System.out.println("Sem permiss�o para compras!");
		}
	}

	void retira(int cod) { // (d)
		int i;
		for (i = 0; i < vetor.length; i++)
			if ((vetor[i] != null) && (vetor[i].getCodigo() == cod))
				break;
		if (i == vetor.length)
			return;
		produto p = vetor[i];
		vetor[i] = null;
		num_prod--;
		peso_atual -= p.getPeso();
		gasto_atual -= p.getPreco();
	}

	public String toString() { // (e )
		String resp = "Mochila de  " + num_prod + " de  " + peso + " kg e total de  " + gasto + ": restantes "
				+ peso_atual + " kg e R$ " + gasto_atual + "\n";
		for (int i = 0; i < vetor.length; i++)
			if (vetor[i] != null)
				resp += vetor[i].toString();
		return resp;
	}
}
